﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HelloWorld
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Hello World!");

            WebHost.CreateDefaultBuilder()
                .Configure(app => app.Run(context => context.Response.WriteAsync("Hello World!")))
                .Build()
                .Run();
        }
    }
}
